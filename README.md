# Como rodar o projeto

- Rodar composer através do comando `docker run --rm --interactive --tty --volume $PWD:/app composer install`

- Subir ambiente com 
    `docker-compose up --build`
- Acessar 0.0.0.0:8000

- Clicar em "Start Championship" (esta etapa pode demorar até 1min)

# Bug
- Alguns "Rounds" não estão sendo persistidos no banco de dados, e no momento não é possível acessá-los pelo botão "See Match Rounds". =(