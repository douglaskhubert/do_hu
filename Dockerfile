FROM php:7.2-alpine3.8
WORKDIR /var/www/html
RUN docker-php-ext-install pdo pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY . /var/www/html
ENTRYPOINT ["bin/console", "server:run", "0.0.0.0:8000"]
