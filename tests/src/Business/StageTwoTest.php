<?php

namespace App\Tests\Business;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StageTwoTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();

        $stageTwo = $container->get('App\Business\StageTwo');
        $winner = $stageTwo->begin();

        $this->assertNotNull($winner);
    }
}
