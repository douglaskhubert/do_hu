<?php

namespace App\Tests\Business;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ChampionshipTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();
        $cleaner = $container->get('App\Business\ClearTables');
        $cleaner->clear();

        $championship = $container->get('App\Business\Championship');
        $winner = $championship->start();

        $this->assertNotNull($winner);
        $this->assertTrue(is_a($winner, "\App\Entity\Team"));
    }
}
