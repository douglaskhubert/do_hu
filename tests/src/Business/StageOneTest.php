<?php

namespace App\Tests\Business;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StageOneTest extends WebTestCase
{
    public function testCreate()
    {
        self::bootKernel();
        $container = self::$kernel->getContainer();
        $cleaner = $container->get('App\Business\ClearTables');
        $cleaner->clear();

        $stageOne = $container->get('App\Business\StageOne');
        $stageTwoTeams = $stageOne->begin();

        $this->assertEquals(16, count($stageTwoTeams));
        foreach ($stageTwoTeams as $group) {
            $this->assertEquals(2, count($group));
        }
    }
}
