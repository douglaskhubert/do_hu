<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScoreStageOneRepository")
 */
class ScoreStageOne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Group")
     */
    private $group;

    /**
     * @ORM\OneToOne(targetEntity="Team")
     */
    private $firstPos;

    /**
     * @ORM\OneToOne(targetEntity="Team")
     */
    private $secondPos;

    /**
     * @ORM\OneToOne(targetEntity="Championship")
     */
    private $championship;

    
    /**
     * Get id.
     *
     * @return id.
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id.
     *
     * @param id the value to set.
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * Get group.
     *
     * @return group.
     */
    public function getGroup()
    {
        return $this->group;
    }
    
    /**
     * Set group.
     *
     * @param group the value to set.
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }
    
    /**
     * Get firstPos.
     *
     * @return firstPos.
     */
    public function getFirstPos()
    {
        return $this->firstPos;
    }
    
    /**
     * Set firstPos.
     *
     * @param firstPos the value to set.
     */
    public function setFirstPos($firstPos)
    {
        $this->firstPos = $firstPos;
        return $this;
    }
    
    /**
     * Get secondPos.
     *
     * @return secondPos.
     */
    public function getSecondPos()
    {
        return $this->secondPos;
    }
    
    /**
     * Set secondPos.
     *
     * @param secondPos the value to set.
     */
    public function setSecondPos($secondPos)
    {
        $this->secondPos = $secondPos;
        return $this;
    }
}
