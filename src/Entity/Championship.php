<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChampionshipRepository")
 */
class Championship
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Team")
     */
    private $winner;


    
    /**
     * Get id.
     *
     * @return id.
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get winner.
     *
     * @return winner.
     */
    public function getWinner()
    {
        return $this->winner;
    }
    
    /**
     * Set winner.
     *
     * @param winner the value to set.
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
    }
}
