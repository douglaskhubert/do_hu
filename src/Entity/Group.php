<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="group")
     * @var Collection
     */
    private $teams;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Match", mappedBy="group")
     */
    private $matches;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
        $this->matches = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Get teams.
     *
     * @return teams.
     */
    public function getTeams()
    {
        return $this->teams;
    }
    
    /**
     * Set teams.
     *
     * @param teams the value to set.
     */
    public function addTeam($team)
    {
        $this->teams->add($team);

        return $this;
    }
    
    /**
     * Get name.
     *
     * @return name.
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set name.
     *
     * @param name the value to set.
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * Get matches.
     *
     * @return matches.
     */
    public function getMatches()
    {
        return $this->matches;
    }
    
    /**
     * Set matches.
     *
     * @param matches the value to set.
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;
    }
}
