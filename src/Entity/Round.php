<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoundRepository")
 */
class Round
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $teamA;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $teamB;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $winner;

    /**
     * @ORM\ManyToOne(targetEntity="Match", inversedBy="rounds", cascade={"persist"})
     */
    private $match;

    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get winner.
     *
     * @return winner.
     */
    public function getWinner()
    {
        return $this->winner;
    }
    
    /**
     * Set winner.
     *
     * @param winner the value to set.
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
        return $this;
    }
    
    /**
     * Get teamA.
     *
     * @return teamA.
     */
    public function getTeamA()
    {
        return $this->teamA;
    }
    
    /**
     * Set teamA.
     *
     * @param teamA the value to set.
     */
    public function setTeamA($teamA)
    {
        $this->teamA = $teamA;
        return $this;
    }
    
    /**
     * Get teamB.
     *
     * @return teamB.
     */
    public function getTeamB()
    {
        return $this->teamB;
    }
    
    /**
     * Set teamB.
     *
     * @param teamB the value to set.
     */
    public function setTeamB($teamB)
    {
        $this->teamB = $teamB;
        return $this;
    }
    
    /**
     * Get match.
     *
     * @return match.
     */
    public function getMatch()
    {
        return $this->match;
    }
    
    /**
     * Set match.
     *
     * @param match the value to set.
     */
    public function setMatch($match)
    {
        $this->match = $match;
        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'teamA' => $this->getTeamA()->getTeamName(),
            'teamB' => $this->getTeamB()->getTeamName(),
            'winner' => $this->getWinner()->getTeamName(),
            'match' => $this->getMatch()->getId()
        ];
    }
}
