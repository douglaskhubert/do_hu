<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchRepository")
 * @ORM\Table(name="`match`")
 */
class Match
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $teamA;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $teamB;

    /**
     * @ORM\Column(type="text", length=10, nullable=true)
     */
    private $teamAScore;

    /**
     * @ORM\Column(type="text", length=10, nullable=true)
     */
    private $teamBScore;

    /**
     * @ORM\OneToMany(targetEntity="Round", mappedBy="match")
     */
    private $rounds;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $winner;

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     */
    private $group;

    public function __construct()
    {
        $this->rounds = new ArrayCollection();
    }

    /**
     * Get teamA.
     *
     * @return teamA.
     */
    public function getTeamA()
    {
        return $this->teamA;
    }
    
    /**
     * Set teamA.
     *
     * @param teamA the value to set.
     */
    public function setTeamA($teamA)
    {
        $this->teamA = $teamA;
        return $this;
    }
    
    /**
     * Get teamB.
     *
     * @return teamB.
     */
    public function getTeamB()
    {
        return $this->teamB;
    }
    
    /**
     * Set teamB.
     *
     * @param teamB the value to set.
     */
    public function setTeamB($teamB)
    {
        $this->teamB = $teamB;
        return $this;
    }

    public function addRound($round)
    {
        $this->rounds->add($round);

        return $this;
    }
    
    /**
     * Get teamAScore.
     *
     * @return teamAScore.
     */
    public function getTeamAScore()
    {
        return $this->teamAScore;
    }
    
    /**
     * Set teamAScore.
     *
     * @param teamAScore the value to set.
     */
    public function setTeamAScore($teamAScore)
    {
        $this->teamAScore = $teamAScore;
        return $this;
    }
    
    /**
     * Get teamBScore.
     *
     * @return teamBScore.
     */
    public function getTeamBScore()
    {
        return $this->teamBScore;
    }
    
    /**
     * Set teamBScore.
     *
     * @param teamBScore the value to set.
     */
    public function setTeamBScore($teamBScore)
    {
        $this->teamBScore = $teamBScore;
        return $this;
    }
    
    /**
     * Get winner.
     *
     * @return winner.
     */
    public function getWinner()
    {
        return $this->winner;
    }
    
    /**
     * Set winner.
     *
     * @param winner the value to set.
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
        return $this;
    }
    
    /**
     * Get group.
     *
     * @return group.
     */
    public function getGroup()
    {
        return $this->group;
    }
    
    /**
     * Set group.
     *
     * @param group the value to set.
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }
    
    /**
     * Get rounds.
     *
     * @return rounds.
     */
    public function getRounds()
    {
        return $this->rounds;
    }
    
    /**
     * Set rounds.
     *
     * @param rounds the value to set.
     */
    public function setRounds($rounds)
    {
        $this->rounds = $rounds;
    }
    
    /**
     * Get id.
     *
     * @return id.
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getRoundsArray()
    {
        if (sizeof($this->rounds) > 1) {
            $arr = [];
            foreach ($this->rounds as $round) {
                $arr[] = $round->toArray();
            }
            return $arr;
        }
        return $this->rounds->toArray();
    }

    /**
     * Set id.
     *
     * @param id the value to set.
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'teamA' => $this->getTeamA(),
            'teamB' => $this->getTeamB(),
            'teamAScore' => $this->getTeamAScore(),
            'teamBScore' => $this->getTeamBScore(),
            'rounds' => $this->getRoundsArray(),
            'winner' => $this->getWinner()->getTeamName(),
            'group' => $this->getGroup()->getName()
        ];
    }
}
