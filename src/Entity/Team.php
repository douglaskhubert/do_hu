<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $teamName;

    /**
     * @ORM\OneToMany(targetEntity="Match", mappedBy="winner")
     */
    private $matchesWon;

    /**
     * @ORM\OneToMany(targetEntity="Round", mappedBy="winner")
     */
    private $roundsWon;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="teams")
     * @ORM\JoinColumn(name="group_id", nullable=true)
     */
    private $group;

    public function __construct()
    {
        $this->matchesWon = new ArrayCollection;
        $this->roundsWon = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getVictories()
    {
        return $this->victories;
    }

    public function setVictories($victories)
    {
        $this->victories = $victories;

        return $this;
    }

    public function getRounds()
    {
        return $this->rounds;
    }

    public function setRounds($rounds)
    {
        $this->rounds = $rounds;

        return $this;
    }

    public function getDefeats()
    {
        return $this->defeats;
    }

    public function setDefeats($defeats)
    {
        $this->defeats = $defeats;

        return $this;
    }
    
    /**
     * Get teamName.
     *
     * @return teamName.
     */
    public function getTeamName()
    {
        return $this->teamName;
    }
    
    /**
     * Set teamName.
     *
     * @param teamName the value to set.
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;
    }
    
    /**
     * Get group.
     *
     * @return group.
     */
    public function getGroup()
    {
        return $this->group;
    }
    
    /**
     * Set group.
     *
     * @param group the value to set.
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }
    
    /**
     * Get matchesWon.
     *
     * @return matchesWon.
     */
    public function getMatchesWon()
    {
        return $this->matchesWon;
    }
    
    /**
     * Set matchesWon.
     *
     * @param matchesWon the value to set.
     */
    public function addMatchWon($matchWon)
    {
        $this->matchesWon[] = $matchWon;
        return $this;
    }
    
    /**
     * Get roundsWon.
     *
     * @return roundsWon.
     */
    public function getRoundsWon()
    {
        return $this->roundsWon;
    }
    
    /**
     * Set roundsWon.
     *
     * @param roundsWon the value to set.
     */
    public function addRoundWon($roundWon)
    {
        $this->roundsWon[] = $roundWon;
        return $this;
    }
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'teamName' => $this->getTeamName(),
            'group' => $this->getGroup()->getName()
        ];
    }
}
