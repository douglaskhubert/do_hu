<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('start.html.twig', [
            'variavel_1' => "Um dois tres"
        ]);
    }


    /**
     * @Route("/championship")
     */
    public function championship(\App\Business\ChampionshipFetcher $championshipFetcher)
    {
        $championship = $championshipFetcher->fetch();

        $winner = $championship->getWinner();
        $response['winner']['id'] = $winner->getId();
        $response['winner']['name'] = $winner->getTeamName();
        $response['winner']['matches_victories'] = count($winner->getMatchesWon());
        $response['winner']['rounds_victories'] = count($winner->getRoundsWon());

        return $this->render('winner.html.twig', [
            'response' => $response
        ]);
    }


    /**
     * @Route("/championship/new")
     */
    public function newChampionship(
        \App\Business\Championship $championship,
        \App\Business\ClearTables $cleaner
    ) {
        set_time_limit(0);
        $cleaner->clear();
        $winner = $championship->start();
        $response['winner']['id'] = $winner->getId();
        $response['winner']['name'] = $winner->getTeamName();

        
        $response['winner']['matches_victories'] = count($winner->getMatchesWon());
        $response['winner']['rounds_victories'] = count($winner->getRoundsWon());

        return $this->render('winner.html.twig', [
            'response' => $response
        ]);
    }

    /**
     * @Route("/championship/details")
     */
    public function matches(
        \App\Business\MatchesFetcher $matchesFetcher
    ) {
        $response = [];
        $allMatches = $matchesFetcher->fetchAll();

        $totalRounds = 0;
        $i = 0;
        foreach ($allMatches as $match) {
            $response["matches"][$match->getId()]["group"] = "Elimination";
            $response["matches"][$match->getId()]["teamATdCssClass"] = "table-danger";
            $response["matches"][$match->getId()]["teamBTdCssClass"] = "table-success";
            $response["matches"][$match->getId()]["teamA"] = $match->getTeamA()->getTeamName();
            $response["matches"][$match->getId()]["teamAScore"] = $match->getTeamAScore();
            $response["matches"][$match->getId()]["teamB"] = $match->getTeamB()->getTeamName();
            $response["matches"][$match->getId()]["teamBScore"] = $match->getTeamBScore();
            if ($match->getGroup()) {
                $response["matches"][$match->getId()]["group"] = $match->getGroup()->getName();
            }
            $winner = $match->getWinner()->getTeamName();
            if ($winner === $response["matches"][$match->getId()]["teamA"]) {
                $response["matches"][$match->getId()]["teamATdCssClass"] = "table-success";
                $response["matches"][$match->getId()]["teamBTdCssClass"] = "table-danger";
            }
            $response["matches"][$match->getId()]["winner"] = $match->getWinner()->getTeamName();
            foreach ($match->getRounds() as $round) {
                $totalRounds++;
            }
            $i++;
        }
        $response['totalRounds'] = $totalRounds;
        
        return $this->render('championship-details.html.twig', [
            'response' => $response
        ]);
    }

    /**
     * @Route("/championship/details/{matchId}")
     */
    public function rounds(
        \App\Business\RoundsFetcher $roundsFetcher,
        \App\Business\MatchesFetcher $matchesFetcher,
        $matchId
    ) {
        $response = [];
        $match = $matchesFetcher->fetch($matchId);
        $response["teamA"] = $match->getTeamA()->getTeamName();
        $response["teamB"] = $match->getTeamB()->getTeamName();
        $response["teamAScore"] = $match->getTeamAScore();
        $response["teamBScore"] = $match->getTeamBScore();
        $response["winner"] = $match->getWinner()->getTeamName();
        $rounds = $roundsFetcher->fetchRoundsByMatch($matchId);

        $i = 0;
        foreach ($rounds as $round) {
            $response["rounds"][$round->getId()]["teamATdCssClass"] = "table-danger";
            $response["rounds"][$round->getId()]["teamBTdCssClass"] = "table-success";
            $response["rounds"][$round->getId()]["teamA"] = $round->getTeamA()->getTeamName();
            $response["rounds"][$round->getId()]["teamB"] = $round->getTeamB()->getTeamName();
            $response["rounds"][$round->getId()]["winner"] = $round->getWinner()->getTeamName();
            $winner = $round->getWinner()->getTeamName();
            if ($winner === $response["rounds"][$round->getId()]["teamA"]) {
                $response["rounds"][$round->getId()]["teamATdCssClass"] = "table-success";
                $response["rounds"][$round->getId()]["teamBTdCssClass"] = "table-danger";
            }
            $i++;
        }
        
        return $this->render('rounds.html.twig', [
            'response' => $response
        ]);
    }
}
