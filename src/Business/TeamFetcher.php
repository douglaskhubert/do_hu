<?php

namespace App\Business;

class TeamFetcher
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public function fetch($id)
    {
        return $this->em->getRepository('App\Entity\Team')->find($id);
    }
}
