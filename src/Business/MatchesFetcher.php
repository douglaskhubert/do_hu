<?php

namespace App\Business;

class MatchesFetcher
{
    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $em
    ){
        $this->em = $em;
    }

    public function fetchAll()
    {
        return $this->em->getRepository('App\Entity\Match')->findBy(array(), array("group" => "ASC"));
    }

    public function fetch($id)
    {
        return $this->em->getRepository('App\Entity\Match')->find($id);
    }
}
