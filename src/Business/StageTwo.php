<?php

namespace App\Business;

class StageTwo implements ChampionshipStageInterface
{
    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $em,
        \App\Business\StageOne $stageOne,
        \App\Business\MatchHandlerFactory $matchHandlerFactory
    ) {
        $this->em = $em;
        $this->stageOne = $stageOne;
        $this->matchHandlerFactory = $matchHandlerFactory;
    }
    public function begin()
    {
        /* Get stage one score */
        $stageOneScore = $this->stageOne->getFinalScore();

        $teams = [];
        foreach ($stageOneScore as $row) {
            $teams[] = $row->getFirstPos();
            $teams[] = $row->getSecondPos();
        }
        shuffle($teams);

        $sixthFinals = array_chunk($teams, 2);
        $eighthFinals = array_chunk($this->play($sixthFinals), 2);
        $fourthFinals = array_chunk($this->play($eighthFinals), 2);
        $semiFinal = array_chunk($this->play($fourthFinals), 2);
        $final = array_chunk($this->play($semiFinal), 2);
        $winner = $this->play($final);

        return $winner[0];
    }

    private function play($matches)
    {
        //TODO - Persist in StageTwoScore (?)
        $nextBracketStage = [];
        foreach ($matches as $match) {
            $matchHandler = $this->matchHandlerFactory->make();
            $matchObj = $matchHandler->createMatchBetween($match[0], $match[1]);
            $nextBracketStage[] = $matchObj->getWinner();
            
        }

        return $nextBracketStage;
    }
}
