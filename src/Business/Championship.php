<?php

namespace App\Business;

class Championship
{
    public function __construct(
        \App\Business\StageOne $stageOne,
        \App\Business\StageTwo $stageTwo,
        \Doctrine\ORM\EntityManagerInterface $em
    ) {
        $this->stageOne = $stageOne;
        $this->stageTwo = $stageTwo;
        $this->em = $em;
    }

    public function start()
    {
        $stageTwoTeams = $this->stageOne->begin();
        $championshipWinner = $this->stageTwo->begin();

        $championship = new \App\Entity\Championship();
        $championship->setWinner($championshipWinner);

        $this->em->persist($championship);
        $this->em->flush();

        return $championshipWinner;
    }
}
