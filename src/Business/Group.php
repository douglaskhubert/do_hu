<?php

namespace App\Business;

class Group
{
    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    public function createGroups($howManyTeamsPerGroup, $teams)
    {
        $groupsArr = array_chunk($teams, $howManyTeamsPerGroup, false);
        $groupsObj = [];
        $i = 1;
        foreach ($groupsArr as $group) {
            $newGroup = new \App\Entity\Group();
            foreach ($group as $team) {
                $newGroup->addTeam($team);
                $team->setGroup($newGroup);
            }
            $newGroup->setName("Group" . $i);
            $this->em->persist($newGroup);
            $this->em->flush();
            $groupsObj[$i] = $newGroup;
            $i++;
        }

        return $groupsObj;
    }

    public function firstTwo($group)
    {
        return $this->em->getRepository('App\Entity\Group')->findFirstTwoTeams($group);
    }
}
