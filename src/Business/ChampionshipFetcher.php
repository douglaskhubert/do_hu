<?php

namespace App\Business;

class ChampionshipFetcher
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public function fetch()
    {
        $championship = $this->em->getRepository('App\Entity\Championship')->findAll();

        return $championship[0];
    }
}
