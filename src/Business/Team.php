<?php

namespace App\Business;

class Team
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create($name)
    {
        $team = new \App\Entity\Team();
        $team->setTeamName($name);
        $this->em->persist($team);
        $this->em->flush();

        return $team;
    }

    public function createAllTeams($howManyTeams = 80)
    {
        $teams = [];
        for ($i = 1; $i <= $howManyTeams; $i++) {
            $teamName = 'Team ' . $i;
            $teams[] = $this->create($teamName);
        }

        return $teams;
    }
}
