<?php

namespace App\Business;

class MatchHandler
{
    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $em,
        RoundHandler $roundHandler
    ){
        $this->em = $em;
        $this->roundHandler = $roundHandler;
        $this->teamAId = null;
        $this->teamBId = null;
        $this->teamAScore = 0;
        $this->teamBScore = 0;
    }

    public function matchEachOther($group)
    {
        $groupMatches = [];
        $matchesToHappen = $this->prepareMatches($group);
        foreach ($matchesToHappen as $m) {
            $match = $this->createMatchBetween(
                $m["teamA"],
                $m["teamB"],
                $group
            );
            $groupMatches[] = [
                "teamA" => $match->getTeamA()->getId(),
                "teamB" => $match->getTeamB()->getId(),
                "winner" => $match->getWinner()->getId()
            ];
            $winner = $match->getWinner();
            $winner->addMatchWon($match);
            $this->em->persist($winner);
            $this->em->flush();
        }

        return $groupMatches;
    }

    public function createMatchBetween($teamA, $teamB, $group = null)
    {
        $this->teamAId = $teamA->getId();
        $this->teamBId = $teamB->getId();

        $match = new \App\Entity\Match();
        $match->setTeamA($teamA)
            ->setTeamB($teamB)
        ;
        /* Playing Match's rounds */
        $rounds = [];
        while (!$this->doWeHaveAWinner()) {
            $round = $this->roundHandler->create($match);
            $match->addRound($round);
            $this->addPointFor($round->getWinner()->getId());
        }
        $winner = $this->whoIsTheMatchWinner($match);
        $match->setWinner($winner)
            ->setTeamAScore($this->teamAScore)
            ->setTeamBScore($this->teamBScore)
        ;
        if ($group) {
            $match->setGroup($group);
        }
        $this->em->persist($match);
        $this->em->flush();

        return $match;
    }

    private function addRoundsInMatch($match, $rounds)
    {
        foreach ($rounds as $round) {
            $match->addRound($round);
        }
        return $match;
    }

    private function addPointFor($teamId)
    {
        return ($this->teamAId === $teamId) ?
            $this->teamAScore++ :
            $this->teamBScore++
        ;
    }
    
    private function whoIsTheMatchWinner($match)
    {
        $winner = $match->getTeamA();
        if ($this->teamBScore === 16) {
            $winner = $match->getTeamB();
        }
        return $winner;
    }

    private function doWeHaveAWinner()
    {
        return ($this->teamAScore === 16) ||
            ($this->teamBScore === 16)
        ;
    }

    private function prepareMatches($group)
    {
        $matchesToHappen = [];
        $teams = $group->getTeams()->getValues();

        for ( $i = 0; $i < sizeof($teams); $i++) {
            $teamA = $teams[$i];
            for ($j = $i+1; $j < sizeof($teams); $j++) {
                $teamB = $teams[$j];
                $matchesToHappen[] = [ "teamA" => $teamA, "teamB" => $teamB ];
            }
        }

        return $matchesToHappen;
    }
}
