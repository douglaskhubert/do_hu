<?php

namespace App\Business;

class StageOne implements ChampionshipStageInterface
{
    const TEAMS_PER_GROUP = 5;

    public function __construct(
        \App\Business\Team $team,
        \App\Business\TeamFetcher $teamFetcher,
        \App\Business\Group $group,
        \App\Business\MatchHandlerFactory $matchHandlerFactory,
        \Doctrine\ORM\EntityManagerInterface $em
    ) {
        $this->team = $team;
        $this->teamFetcher = $teamFetcher;
        $this->group = $group;
        $this->matchHandlerFactory = $matchHandlerFactory;
        $this->em = $em;
    }

    public function begin()
    {
        /* Creating Teams */
        $teams = $this->team->createAllTeams();

        /* Randomizing Teams */
        shuffle($teams);
        
        /* Creating Groups */
        $groups = $this->group->createGroups(
            self::TEAMS_PER_GROUP,
            $teams
        );

        /* Playing Matches */
        $matches = [];
        foreach ($groups as $group) {
            $matchHandler = $this->matchHandlerFactory->make();
            $matches[$group->getName()][] = $matchHandler->matchEachOther($group);
        }

        $firstTwoTeamsByGroup = [];
        foreach ($groups as $group) {
            $teamsFromThisGroup = $this->group->firstTwo($group);
            $stageTwoTeams[$group->getName()] = $this->group->firstTwo($group);
            $firstPos = $this->teamFetcher->fetch($teamsFromThisGroup[0]['teamId']);
            $secondPos = $this->teamFetcher->fetch($teamsFromThisGroup[1]['teamId']);

            $scoreRow = new \App\Entity\ScoreStageOne();
            $scoreRow->setGroup($group)
                ->setFirstPos($firstPos)
                ->setSecondPos($secondPos)
            ;
            $this->em->persist($scoreRow);
            $this->em->flush();
        }

        return $stageTwoTeams;
    }
    public function getFinalScore()
    {
        return $this->em->getRepository('App\Entity\ScoreStageOne')->findAll();
    }
}
