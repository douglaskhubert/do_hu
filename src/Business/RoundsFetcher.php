<?php

namespace App\Business;

class RoundsFetcher
{
    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $em
    ){
        $this->em = $em;
    }

    public function fetchRoundsByMatch($matchId)
    {
        return $this->em->getRepository('App\Entity\Round')->findBy(array('match' => $matchId ));
    }
}
