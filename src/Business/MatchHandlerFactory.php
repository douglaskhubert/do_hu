<?php

namespace App\Business;

class MatchHandlerFactory
{
    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $em,
        \App\Business\RoundHandler $roundHandler
    ){
        $this->em = $em;
        $this->roundHandler = $roundHandler;
    }
    public function make()
    {
        return new \App\Business\MatchHandler(
            $this->em,
            $this->roundHandler
        );
    }
}
