<?php

namespace App\Business;

class RoundHandler
{
    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public function create($match)
    {
        $winner = (rand(0, 10) <= 5) ?
                $match->getTeamA() :
                $match->getTeamB()
        ;
        $round = new \App\Entity\Round();
        $round->setTeamA($match->getTeamA())
            ->setTeamB($match->getTeamB())
            ->setWinner($winner)
            ->setMatch($match)
        ;
        $this->em->persist($round);
        $this->em->flush();

        $winner->addRoundWon($round);
        $this->em->persist($winner);
        $this->em->flush();

        return $round;
    }
}
