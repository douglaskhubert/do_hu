<?php

namespace App\Repository;

use App\Entity\Group;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Group|null find($id, $lockMode = null, $lockVersion = null)
 * @method Group|null findOneBy(array $criteria, array $orderBy = null)
 * @method Group[]    findAll()
 * @method Group[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Group::class);
    }
    public function findFirstTwoTeams($group)
    {
        $em = $this->getEntityManager();
        $dqlString = <<<DQL
            SELECT 
                t.id as teamId, 
                t.teamName, 
                COUNT(m.id) as victories 
            FROM \App\Entity\Team t
            INNER JOIN \App\Entity\Match m
            WHERE t.group = :groupId AND m.winner = t.id
            GROUP BY t.id
            ORDER BY victories DESC
DQL;
        $query = $em->createQuery($dqlString);
        $query->setParameter('groupId', $group->getId());
        $teams = $query->getResult();

        return [ $teams[0], $teams[1] ];
    }
    // /**
    //  * @return Group[] Returns an array of Group objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Group
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
