<?php

namespace App\Repository;

use App\Entity\ScoreStageOne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ScoreStageOne|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScoreStageOne|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScoreStageOne[]    findAll()
 * @method ScoreStageOne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreStageOneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ScoreStageOne::class);
    }

    // /**
    //  * @return ScoreStageOne[] Returns an array of ScoreStageOne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScoreStageOne
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
