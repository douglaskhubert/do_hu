<?php

namespace App\Repository;

use App\Entity\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Team::class);
    }

    public function findStageOneClassificateds()
    {
        $em = $this->getEntityManager();
        $dqlString = <<<DQL
            SELECT 
                t.id as teamId, 
                t.teamName, 
                COUNT(m.id) as victories 
            FROM \App\Entity\Team t
            INNER JOIN \App\Entity\Match m
            WHERE t.group = :groupId AND m.winner = t.id
            GROUP BY t.id
            ORDER BY victories DESC
DQL;
        $query = $em->createQuery($dqlString);
        $query->setParameter('groupId', $group->getId());
        $teams = $query->getResult();
        
        $team2MatchesWon = count($teams[1]->getMatchesWon());
        $team3MatchesWon = count($teams[2]->getMatchesWon());
        if ($team2MatchesWon === $team3MatchesWon) {
            $roundsWonTeam2 = count($teams[1]->getRoundsWon());
            $roundsWonTeam3 = count($teams[2]->getRoundsWon());
            if ($roundsWonTeam3 > $roundsWonTeam2) {
                $teams[1] = $teams[2];
            }
        }

        return [ $teams[0], $teams[1] ];
    }

    // /**
    //  * @return Team[] Returns an array of Team objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Team
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
